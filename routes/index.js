var express = require('express');
var router = express.Router();
var fs= require('fs');

// Mongoose import
var mongoose = require('mongoose');

// Mongoose connection to MongoDB
mongoose.connect('mongodb://localhost:27017/leafletDB', function (error) {
    if (error) {
        console.log(error);
    }
});


// Mongoose Schema definition
var Schema = mongoose.Schema;
var PositionSchema = new Schema({
    lat: Schema.Types.String,
    lon: Schema.Types.String
});
var VisitorSchema = new Schema({
    _id: Schema.Types.Number,
    interestNeeded: Schema.Types.String,
    interestReduce: Schema.Types.String,
    coordinates: [PositionSchema],
    correctedPosition:[PositionSchema]
});

var StationSchema = new Schema({
    radius: Schema.Types.Number,
    accuracy:Schema.Types.Number,
    position:PositionSchema
});

var ClusterSchema = new Schema({
    name: Schema.Types.String,
    position: PositionSchema,
    weight:Schema.Types.Number,
    distance:Schema.Types.Number
});



var JsonSchema = new Schema({
    name: String,
    type: Schema.Types.Mixed
});

// Mongoose Model definition


var Visitor = mongoose.model('Visitor', VisitorSchema, 'visitors');

var Station = mongoose.model('Station', StationSchema, 'stations');

var Cluster = mongoose.model('Cluster', ClusterSchema, 'cluster');

var Json = mongoose.model('JString', JsonSchema, 'layercollection');
/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {title: 'Express'});
});

router.get('/doc', function(req, res, next) {
  var file = fs.createReadStream('main.pdf');
  var stat = fs.statSync('main.pdf');
  res.setHeader('Content-Length', stat.size);
  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader('Content-Disposition', 'attachment; filename=quote.pdf');
  file.pipe(res);
});

/* GET json data. */

router.get('/mapjson/visitors', function (req, res) {
    var response = [];
    var visitorArray = [];
    Visitor.find().lean().exec(function (err, visitors) {
        for (var i = 0; i < visitors.length; i++) {
            response.push({_id: visitors[i]._id, name: "Visitors", coordinates: visitors[i].position,correctedPosition:visitors[i].correctedPosition});
        }
        res.json(response);
    });

});

router.get('/mapjson/stations', function (req, res) {
    var response = [];
    var stationArray = [];
    Station.find().lean().exec(function (err, stations) {
        for (var i = 0; i < stations.length; i++) {
            response.push({position: stations[i].position,radius:stations[i].radius,accuracy:stations[i].accuracy});
        }
        res.json(response);
    });
});

router.get('/mapjson/cluster', function (req, res) {
    var response = [];
    var clusterArray = [];
    Cluster.find().lean().exec(function (err, cluster) {
        for (var i = 0; i < cluster.length; i++) {
            response.push({position: cluster[i].position,name:cluster[i].name,distance:cluster[i].distance,weight:cluster[i].weight});
        }
        res.json(response);
    });

});

router.get('/mapjson/visitors/:i', function (req, res) {
    var response = [];
    var visitorArray = [];
    var i=req.params.i;
    Visitor.find().lean().exec(function (err, visitors) {
        response.push({_id: visitors[i]._id, name: "Visitors", coordinates: visitors[i].position});
        res.json(response);
    });

});

/* GET json data. */
router.get('/mapjson/:name', function (req, res) {
    if (req.params.name) {
        Json.findOne({name: req.params.name}, {}, function (err, docs) {
            res.json(docs);
        });
    }
});

/* GET layers json data. */
router.get('/maplayers', function (req, res) {
    Json.find({}, {'name': 1}, function (err, docs) {
        res.json(docs);
    });
});

/* GET Map page. */
router.get('/map', function (req, res) {
    var db = req.db;
    Json.find({}, {}, function (err, docs) {
        res.render('map', {
            "jmap": docs,
            lat: 50.688151,
            lng: 10.944551
        });
    });
});

module.exports = router;
